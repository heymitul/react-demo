FROM node:latest


# set working directory
WORKDIR /usr/src/app

# install and cache app dependencies
COPY package.json ./
ADD package.json /usr/src/app/package.json
RUN npm install


# bundle up the source
COPY . .


# specify port
EXPOSE 3000

#start app
CMD [ "npm", "start" ]
