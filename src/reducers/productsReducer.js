import { FETCH_PRODUCTS, STOCK_PRODUCTS, FILTER_PRODUCTS } from '../actions/types';

/**
 * Initial state
 */
const initialState = {
    products: [],
    filteredProducts: [],
    query: '',
    showStockedOnlyProducts: false
}

/**
 * 
 * Filter products based on params provided.
 * 
 * @param {*} queryStr search string 
 * @param {*} showStocked if true, shows stocked products else shows all products
 * @param {*} state current redux state
 */
const filterProducts = (queryStr, showStocked, state) => {
    if (queryStr.length > 0 && showStocked) {
        return state.products.filter(i => i.name.toLowerCase().indexOf(queryStr.toLowerCase()) === 0 && i.stocked === showStocked);
    }

    if (queryStr.length === 0 && showStocked) {
        return state.products.filter(i => i.stocked === showStocked);
    }

    if (queryStr.length > 0 && !showStocked) {
        return state.products.filter(i => i.name.toLowerCase().indexOf(queryStr.toLowerCase()) === 0);
    }

    return state.products;
}

export default function (state = initialState, action) {
    let showStocked = false;
    let queryStr = '';

    let filteredProducts = [];

    switch (action.type) {
        case FETCH_PRODUCTS:
            return { ...state, products: action.payload };
        case STOCK_PRODUCTS:
            showStocked = action.payload;
            queryStr = state.query;

            filteredProducts = filterProducts(queryStr, showStocked, state);
            return { ...state, showStockedOnlyProducts: showStocked, query: queryStr, filteredProducts: filteredProducts };
        case FILTER_PRODUCTS:
            showStocked = state.showStockedOnlyProducts;
            queryStr = action.payload;

            filteredProducts = filterProducts(queryStr, showStocked, state);
            return { ...state, showStockedOnlyProducts: showStocked, query: queryStr, filteredProducts: filteredProducts };
        default:
            return { ...initialState };
    }
}