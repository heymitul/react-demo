//import 'materialize-css/dist/css/materialize.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, compose } from 'redux';
import productsReducer from "./reducers/productsReducer";
import reduxThunk from 'redux-thunk';

import App from './components/App';

const composeEnhancers =
    typeof window === 'object' &&
        (window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        ? (window).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({})
        : compose;

const store = createStore(productsReducer, {}, composeEnhancers(applyMiddleware(reduxThunk)));

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector("#root")
);