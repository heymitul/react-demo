import axios from "axios";
import { FETCH_PRODUCTS, STOCK_PRODUCTS, FILTER_PRODUCTS } from "./types";

/**
 * Fetch products from API
 */
export const fetchProducts = () => async dispatch => {
    const res = await axios.get('https://api.myjson.com/bins/109m7i');
    dispatch({ type: FETCH_PRODUCTS, payload: res.data });
};

/**
 * dispatch action for filter to show stocked only products or all products
 */
export const showStockedProduct = (flag) => dispatch => {
    dispatch({ type: STOCK_PRODUCTS, payload: flag });
}

/**
 * dispatch actions for search.
 */
export const showFilterProduct = (query) => dispatch => {
    dispatch({ type: FILTER_PRODUCTS, payload: query });
}