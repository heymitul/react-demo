export const FETCH_PRODUCTS = 'fetch_products';
export const STOCK_PRODUCTS = 'stock_products';
export const FILTER_PRODUCTS = 'filter_products';