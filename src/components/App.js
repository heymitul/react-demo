import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import * as _ from 'lodash';

import './style.css';

class App extends Component {

    componentDidMount() {
        this.props.fetchProducts();
    }

    renderItems(items: []) {
        return items.map(({ name, price, stocked }, index) => {

            return <tr key={index}>
                <td style={{ color: !stocked ? 'red' : '#000' }}>
                    {name}
                </td>
                <td>
                    {price}
                </td>
            </tr>
        });
    }

    render() {
        let productList = (this.props.query && this.props.query.length > 0) || this.props.showStockedOnlyProducts ? this.props.filteredProducts : this.props.products;
        let products = _.chain(productList).groupBy("category").map((items, category) => ({ category, items })).value();
        return (
            <div className="container-block">
                <div>
                    <input className="search-input-container" type="text" placeholder="Search..." onChange={(e) => this.props.showFilterProduct(e.target.value)} />
                </div>
                <div className="show-only-stocked-products">
                    <input type="checkbox" onChange={(e) => this.props.showStockedProduct(e.target.checked)} /> Only show products in stock
                </div>
                <table id='productTable'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    {
                        products ? products.map(({ category, items }, index) => {
                            return (
                                <tbody key={index}>
                                    <tr className="header">
                                        <th colSpan="2">
                                            {category}
                                        </th>
                                    </tr>
                                    {this.renderItems(items)}
                                </tbody>)
                        }) : null
                    }
                </table>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        products: state.products,
        filteredProducts: state.filteredProducts,
        query: state.query,
        showStockedOnlyProducts: state.showStockedOnlyProducts
    }
}

let app = connect(mapStateToProps, actions)(App);
export default app;